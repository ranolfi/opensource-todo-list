Hi. I'm Marc Ranolfi, an independent cross-platform software developer living in Curitiba, Brazil. I'm most known around the internet by my decade-long username *marc.2377*.

I'm not a bug hunter - and never quite wanted to be -, but I've been rather ~~haunted~~ *hunted* by bugs since I set out to use GNU/Linux as my default operating system, back some 2 years ago.

This is a "meta-project" where I'm compiling them all, as well as suggested improvements to software I use daily, and also some initiatives of my own interest regarding free and open source software. It is part of an ongoing effort to improve the GNU/Linux experience overall.

I'm going to begin tackling on these as soon as I figure out a way to *not* having to work most of my time for a living. Which should happen very soon.

You are most welcome to take on a task by yourself, or/and report it upstream if it wasn't already; but for most of them, it's likely not a priority for the project developers. Which is why some of them have persisted for so much time without being addressed.

Enough talking for now; please refer to **https://gitlab.com/ranolfi/opensource-todo-list/issues** for the actual list. But do check back here from time to time, since this is work-in-progress. Thank you.


*2018-07-27 update:* I've recently talked to some good friends and there is a possibility we can get this rolling by setting up a Patreon (or similar) campaign which would allow me to focus on these issues. There are still some issues to be listed, and a few of those already in the list are now fixed upstream; The list should be up-to-date by mid August.
